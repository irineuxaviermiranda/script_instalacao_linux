#!/usr/bin/env bash

sudo apt install build-essential \
                 apt-listchanges \
                 apt-listbugs \
                 curl \
                 lynx
                 xorg
                 gnome-core
                 gnome-shell
                 gnome-shell-extensions
                 gnome-software
                 xdg-utils
                 htop -y
